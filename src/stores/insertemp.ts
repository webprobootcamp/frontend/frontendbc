import type { InsertEmp } from '@/types/InsertEmp'
import { defineStore } from 'pinia'
import { ref } from 'vue'
import { useLoadingStore } from './loading'
import insertemp from '@/services/insertemp'
import { useEmployStore } from './employee'
import type { Employee } from '@/types/Employee'

export const useInsertTempStore = defineStore('insertTemp', () => {
  const loadingStore = useLoadingStore()
  const InsertEmp = ref<InsertEmp[]>([])
  const employeeStore = useEmployStore()

  const createEmployee = async (employee: Employee) => {
    console.log(employee.dateIn.split('T')[0])
    if (employee.status.name === 'FullTime') {
      const res = await insertemp.getAllspp(
        employee.tel,
        employee.name,
        employee.salary,
        employee.dateIn.split('T')[0],
        1 + '',
        1 + '',
        0
      )
      if (res.data) {
        console.log(res.data)
        employeeStore.getEmployees()
      }
    } else {
      const res = await insertemp.getAllspp(
        employee.tel,
        employee.name,
        employee.salary,
        employee.dateIn.split('T')[0],
        1 + '',
        2 + '',
        0
      )
      if (res.data) {
        console.log(res.data)
        employeeStore.getEmployees()
      }
    }
    employeeStore.editedEmployee = {
      dateIn: '',
      files: [],
      image: '',
      name: '',
      salary: 0,
      tel: '',
      status: {
        name: '',
        id: -1
      }
    }
  }
  return {
    InsertEmp,
    createEmployee
  }
})
