import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useAvgMoreStore = defineStore('Avg', () => {
  const snackbar = ref(false)
  const text = ref('')
  function showMessage(msg: string) {
    text.value = msg
    snackbar.value = true
  }
  return { showMessage, snackbar, text }
})
