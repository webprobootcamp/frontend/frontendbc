import { useLoadingStore } from './loading'
import { ref } from 'vue'
import { defineStore } from 'pinia'
import receiptService from '@/services/receipt'

export const useRangeDateReceiptStore = defineStore('RangeDateReceipt', () => {
  const loadingStore = useLoadingStore()
  const receipts = ref([])

  async function getReceiptInRangeDate(type:string, startDate:string, endDate:string) {
    try {
      loadingStore.doLoad()
      console.log("type, startDate, endDate =" + type + startDate + endDate)
      const res = await receiptService.getReceiptInRangeDate(type, startDate, endDate)
      receipts.value = res  // Assuming res is already an array of receipt data
      loadingStore.finish()
    } catch (e) {
      console.error('Error occurred:', e)
      loadingStore.finish()
      // Handle error - perhaps show error message
    }
  }

  return {
    getReceiptInRangeDate,
    receipts
  }
})
