import { ref } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import type { Attendance } from '@/types/Attendance'
import attendanceService from '@/services/attendance'
import { useAuthStore } from './auth'

export const useAttendanceStore = defineStore('Attendance', () => {
  const loadingStore = useLoadingStore()
  const authStore = useAuthStore()
  const attendances = ref<Attendance[]>([])

  const initialAttendance: Attendance = {
    employee: {
      name: '',
      tel: '',
      status: { id: 1, name: 'FullTime' },
      salary: 0,
      dateIn: '',
      image: 'noimage.jpg',
      files: [],
      branch: null
    },
    name: '',
    date: new Date(),
    clockIn: '',
    clockOut: '',
    status: '',
    workedTime: 0
  }
  const userAttendance = ref<Attendance>(JSON.parse(JSON.stringify(initialAttendance)))

  async function getAttendance(id: number) {
    loadingStore.doLoad()
    const res = await attendanceService.getAttendance(id)
    userAttendance.value = res.data
    loadingStore.finish()
  }

  async function getAllAttendance() {
    loadingStore.doLoad()
    const res = await attendanceService.getAllAttendance()
    // attendances.value = res.data
    attendances.value = res.data
    console.log(res.data)
    loadingStore.finish()
  }
  async function getAllAttendanceByEmployeeId(employeeId: number) {
    loadingStore.doLoad()
    const res = await attendanceService.getAttendanceByEmployeeId(employeeId)
    console.log(res.data)

    if (res.data) {
      attendances.value = res.data
    }
    loadingStore.finish()
  }

  async function clockIn() {
    loadingStore.doLoad()
    const attendance = userAttendance.value
    console.log(attendance)
    console.log(attendance.id)
    if (!attendance.id) {
      // Add new attendance
      console.log('Post', JSON.stringify(attendance))
      const res = await attendanceService.clockIn(attendance.employee)
    }
    loadingStore.finish()
  }

  async function clockOut() {
    loadingStore.doLoad()
    const attendance = userAttendance.value
    console.log(attendance.id)

    // if (attendance.id) {
    // Update existing attendance
    console.log('Patch', JSON.stringify(attendance))
    const res = await attendanceService.clockOut(authStore.getCurrentUser()?.employee?.id!)
    console.log(res.data)
    // } else {
    //   console.log('notworking')
    // }
    loadingStore.finish()
  }
  return {
    clockIn,
    clockOut,
    attendances,
    userAttendance,
    getAllAttendance,
    getAttendance,
    getAllAttendanceByEmployeeId
  }
})
