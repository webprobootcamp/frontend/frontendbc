import type { CountEmp } from '@/types/CountEmp'
import { defineStore } from 'pinia'
import { ref } from 'vue'
import { useLoadingStore } from './loading'
import countemp from '@/services/countemp'
import { useEmployStore } from './employee'
import type { Employee } from '@/types/Employee'

export const useCountEmpTempStore = defineStore('LoopEmployCounter', () => {
  const loadingStore = useLoadingStore()
  const CountEmps = ref([])
  const countEmpStore = useEmployStore()

  type typeEmp = {
    name: string
    tel: string
    salary: number
    dateIn: string
    statusId: string
    branchId: string
  }
  const ctem = ref<typeEmp>({
    name: '',
    tel: '',
    salary: 0,
    dateIn: '',
    statusId: '',
    branchId: ''
  })

  const createCountEmployee = async (employee: typeEmp) => {
    console.log(employee.dateIn.split('T')[0])

    const res = await countemp.getAllsppp(
      employee.tel,
      employee.name,
      employee.salary,
      employee.dateIn,
      employee.statusId,
      employee.branchId,
      0
    )
    if (res.data) {
      console.log(res.data)
      countEmpStore.getEmployees()
    }

    countEmpStore.editedEmployee = {
      dateIn: '',
      files: [],
      image: '',
      name: '',
      salary: 0,
      tel: '',
      status: {
        name: '',
        id: -1
      }
    }
  }
  return {
    CountEmps,
    createCountEmployee,
    ctem,
    loadingStore,
    countEmpStore
  }
})
