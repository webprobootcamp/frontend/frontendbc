import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Product } from '@/types/Product'
import productService from '@/services/product'
import { useLoadingStore } from './loading'

export const usePosStore = defineStore('pos', () => {
  const loadingStore = useLoadingStore()

  const products1 = ref<Product[]>([])
  const products2 = ref<Product[]>([])
  const products3 = ref<Product[]>([])
  const products4 = ref<Product[]>([])

  async function getProducts() {
    try {
      loadingStore.doLoad()
      let res = await productService.getProductsbyType(1)
      console.log(res.data)
      products1.value = res.data
      res = await productService.getProductsbyType(2)
      products2.value = res.data
      res = await productService.getProductsbyType(3)
      products3.value = res.data
      res = await productService.getProductsbyType(4)
      products4.value = res.data
      loadingStore.finish()
    } catch (e) {
      console.log('Error')
      loadingStore.finish()
    }
  }
  return { products1, products2, products3, getProducts, products4 }
})
