// import type { ReportExpenses } from "@/types/ReportExpenses";
// import type { ReportExpensesItem } from "@/types/ReportExpensesItem";
// import { defineStore } from "pinia";
// import { ref } from "vue";
// import { useBranchStore } from "./branch";
// import { useAuthStore } from "./auth";

// export const useReportExpensesStore = defineStore('reportexpenses', () => {
//     const authStore = useAuthStore();
//     const branchStore = useBranchStore();

//     const reportExpenses = ref<ReportExpenses>({
//         id: 0,
//         createdDate: new Date(),
//         branch: branchStore.getBranch(),
//         employees: authStore.getCurrentUser(),
//         reportexpensesitem: [],
//     });

//     const reportexpensesitem = ref<ReportExpensesItem[]>([]);

//     const addReportExpensesItem = (item: ReportExpensesItem) => {
//         // เพิ่มรายการลงใน reportexpensesitem
//         reportexpensesitem.value.push(item);
//     }

//     return {
//         reportExpenses,
//         reportexpensesitem,
//         addReportExpensesItem,
//     };
// });
