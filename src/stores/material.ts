import type { Material } from '@/types/Material'
import { defineStore } from 'pinia'
import { ref } from 'vue'
import { useLoadingStore } from './loading'
import meterial from '@/services/meterial'
import { useStockStore } from './stock'

export const useMaterialTempStore = defineStore('GetMaterialInfo', () => {
  const loadingStore = useLoadingStore()
  const Materials = ref<Material[]>([])
  const materialStore = useStockStore()
  type typeMat = { status: String; unit: Number }
  const mobj = ref<typeMat>({ status: '', unit: 0 })

  const createMaterial = async () => {
    try {
      const res = await meterial.getAllspp(mobj.value)
      if (res.data) {
        Materials.value = res.data
      }
      Materials.value.push(res.data)
    } catch (error) {
      console.log(error)
    }
  }
  return { Materials, materialStore, loadingStore, createMaterial, mobj }
})
