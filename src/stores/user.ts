import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import userService from '@/services/user'
import type { User } from '@/types/User'
import { useMessageStore } from './message'

export const useUserStore = defineStore('user', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const users = ref<User[]>([])
  const initialUser: User & { files: File[] } = {
    email: '',
    password: '',
    name: '',
    gender: 'male',
    role: { id: 1, name: 'user' },
    image: 'noimage.jpg',
    files: [],
    employee: {
      "name": "",
      "tel": "",
      "status": {
          "id": 1,
          "name": "FullTime"
      },
      "salary": 0,
      "dateIn": "",
      "image": "noimage.jpg",
      "files": [],
      "branch": null
  }
  }
  const editedUser = ref<User & { files: File[] }>(JSON.parse(JSON.stringify(initialUser)))
  const currentUser = ref()
  async function getUser(id: number) {
    loadingStore.doLoad()
    const res = await userService.getUser(id)
    editedUser.value = res.data
    loadingStore.finish()
  }
  async function getUsers() {
    loadingStore.doLoad()
    const res = await userService.getUsers()
    users.value = res.data
    loadingStore.finish()
  }
  async function saveUser() {
    try {
      loadingStore.doLoad()
      const user = editedUser.value
      if (!user.id) {
        // Add new
        console.log('Post ' + JSON.stringify(user))
        const res = await userService.addUser(user)
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(user))
        const res = await userService.updateUser(user)
      }

      await getUsers()
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }
  async function deleteUser() {
    loadingStore.doLoad()
    const user = editedUser.value
    const res = await userService.delUser(user)

    await getUsers()
    loadingStore.finish()
  }

  function clearForm() {
    editedUser.value = JSON.parse(JSON.stringify(initialUser))
  }
  return { users, getUsers, saveUser, deleteUser, editedUser, getUser, clearForm, currentUser }
})
