import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import receiptService from '@/services/receipt'
import { useMessageStore } from './message'

export const useOrderMoreAvgStore = defineStore('Avg', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const oderavgs = ref([])

  async function getOrderAvgView() {
    try {
      loadingStore.doLoad()
      const res = await receiptService.getOrderAvgView()
      console.log(res.data)
      oderavgs.value = res.data

      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }

  return {
    getOrderAvgView,
    oderavgs
  }
})
