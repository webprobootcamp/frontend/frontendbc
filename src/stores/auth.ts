import { defineStore } from 'pinia'
import type { User } from '@/types/User'
import authService from '@/services/auth'
import userService from '@/services/user'
import employeeService from '@/services/employee'
import { useMessageStore } from '@/stores/message'
import { useRouter } from 'vue-router'
import { useLoadingStore } from './loading'
import { useUserStore } from './user'
import { useEmployStore } from './employee'
import { useBranchStore } from './branch'
import type { Branch } from '@/types/Branch'
export const useAuthStore = defineStore('auth', () => {
  const messageStore = useMessageStore()
  const router = useRouter()
  const loadingStore = useLoadingStore()
  const userStore = useUserStore()
  const employeeStore = useEmployStore()
  const branchStore = useBranchStore()
  async function login(email: string, password: string) {
    loadingStore.doLoad()
    try {
      const res = await authService.login(email, password)
      console.log(res.data)
      localStorage.setItem('user', JSON.stringify(res.data.user))
      localStorage.setItem('access_token', res.data.access_token)
      const currentUser = getCurrentUser()

      const employeeId = currentUser?.employee?.id!
      const employeeData = await employeeService.getEmployee(employeeId)
      employeeStore.currentEmployee = employeeData.data
      console.log('employeeStore.currentEmployee =', employeeStore.currentEmployee)
      console.log('branch =', employeeData.data.branch)
      console.log('employeeData.data.employee.branch', employeeData.data.branch)
      localStorage.setItem('employee', employeeData.data)
      localStorage.setItem('branch', JSON.stringify(employeeData.data.branch))
      router.replace('/pos-view')
      console.log(
        'employeeStore.currentEmployee?.branch =' + employeeStore.currentEmployee?.branch?.id
      )
    } catch (e: any) {
      console.log(e)
      messageStore.showMessage(e.message)
    }
    loadingStore.finish()
  }

  function logout() {
    localStorage.removeItem('user')
    localStorage.removeItem('access_token')
    localStorage.removeItem('employee')
    router.replace('/login')
  }

  function getCurrentUser(): User | null {
    const strUser = localStorage.getItem('user')
    if (strUser === null) return null
    return JSON.parse(strUser)
  }

  function getCurrentBranch(): Branch | null | undefined {
    const strBranch = localStorage.getItem('branch')
    if (strBranch === null) return null
    return JSON.parse(strBranch)
  }

  function getToken(): User | null {
    const strToken = localStorage.getItem('access_token')
    if (strToken === null) return null
    return JSON.parse(strToken)
  }

  return { getCurrentUser, getToken, login, logout, getCurrentBranch }
})
