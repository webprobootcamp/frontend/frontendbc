import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import type { BSPaperItem } from '@/types/BSPaperItem'
import type { BSPaper } from '@/types/BSPaper'
import { useAuthStore } from './auth'
import itembsService from '@/services/itembs'
import { useLoadingStore } from './loading'
import { useMessageStore } from './message'
import type { Stock } from '@/types/Stock'
import stock from '@/services/stock'
import bspaperService from '@/services/bspaper'

export const useBSPaperStore = defineStore('bspaper', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const authStore = useAuthStore()
  const receiptsbuy = ref()
  const DeleteDialog = ref(false)
  const bsPaperItems = ref<BSPaperItem[]>([])
  const bsPaper = ref<BSPaper>({
    id: 0,
    created: new Date(),
    total: 0,
    amount: 0,
    userId: authStore.getCurrentUser()?.id,
    user: authStore.getCurrentUser()
  })
  function initBSPaper() {
    bsPaper.value = {
      id: 0,
      created: new Date(),
      total: 0,
      amount: 0,
      userId: authStore.getCurrentUser()?.id,
      user: authStore.getCurrentUser()
    }
    bsPaperItems.value = []
  }

  watch(
    bsPaperItems,
    () => {
      calBSPaper()
    },
    { deep: true }
  )
  const addBSPaperItem = (b: Stock) => {
    const index = bsPaperItems.value.findIndex((item) => item.stock?.id === b.id)
    if (index >= 0) {
      bsPaperItems.value[index].qty++
      calBSPaper()
      return
    } else {
      const newBSPaperItem: BSPaperItem = {
        id: -1,
        name: b.name,
        price: b.price,
        qty: 1,
        unit: b.unit,
        stockId: b.id,
        stock: b
      }
      bsPaperItems.value.push(newBSPaperItem)
    }
    calBSPaper()
  }
  const calBSPaper = function () {
    bsPaper.value!.total = 0
    bsPaper.value!.amount = 0
    for (let i = 0; i < bsPaperItems.value.length; i++) {
      bsPaper.value!.total += bsPaperItems.value[i].price * bsPaperItems.value[i].qty
      bsPaper.value!.amount += bsPaperItems.value[i].qty
    }
  }
  const editedBSPaper = ref<BSPaper>(JSON.parse(JSON.stringify(bsPaper)))
  async function getBSPaper(id: number) {
    loadingStore.doLoad()
    const res = await bspaperService.getBSPaper(id)
    editedBSPaper.value = await res.data
    loadingStore.finish()
  }

  async function getBSPapers() {
    try {
      loadingStore.doLoad()
      const res = await bspaperService.getBSPapers()
      const receiptbuyData = res.data
      receiptsbuy.value = await receiptbuyData
      loadingStore.finish()
    } catch (error) {
      console.error(error)
      loadingStore.finish()
    }
  }
  // const addBSPaperItem = (newBSPaperItem: BSPaperItem) => {
  //   bsPaperItems.value.push(newBSPaperItem)
  // }
  const deleteBSPaperItem = (selectedItem: BSPaperItem) => {
    const index = bsPaperItems.value.findIndex((item) => item === selectedItem)
    bsPaperItems.value.splice(index, 1)
  }
  const incQtyOfBSPaperItem = (selectedItem: BSPaperItem) => {
    selectedItem.qty++
    calBSPaper()
  }
  const decQtyOfBSPaperItem = (selectedItem: BSPaperItem) => {
    if (selectedItem.qty === 1) {
      deleteBSPaperItem(selectedItem)
      return
    }
    selectedItem.qty--
    calBSPaper()
  }
  const removeItem = (item: BSPaperItem) => {
    const index = bsPaperItems.value.findIndex((ri) => ri === item)
    bsPaperItems.value.splice(index, 1)
  }

  function clearForm() {
    bsPaper.value = {
      id: 0,
      created: new Date(),
      total: 0,
      amount: 0,
      userId: authStore.getCurrentUser()!.id!,
      user: authStore.getCurrentUser()!
    }
    bsPaperItems.value = []
  }

  function showDeleteDialog() {
    bsPaper.value.buystockorderItems = bsPaperItems.value
    DeleteDialog.value = true
  }

  const Buystock = async () => {
    try {
      loadingStore.doLoad()
      await itembsService.addItemBS(bsPaper.value!, bsPaperItems.value)
      initBSPaper()
      loadingStore.finish()
    } catch (e: any) {
      loadingStore.finish()
      messageStore.showMessage(e.message)
    }
  }

  return {
    bsPaper,
    DeleteDialog,
    initBSPaper,
    bsPaperItems,
    addBSPaperItem,
    incQtyOfBSPaperItem,
    decQtyOfBSPaperItem,
    deleteBSPaperItem,
    removeItem,
    Buystock,
    clearForm,
    showDeleteDialog,
    getBSPaper,
    getBSPapers,
    receiptsbuy
  }
})
