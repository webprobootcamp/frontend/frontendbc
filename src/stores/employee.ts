import type { Employee } from '@/types/Employee'
import { defineStore } from 'pinia'
import { onMounted, ref } from 'vue'
import { useLoadingStore } from './loading'
import employeeService from '@/services/employee'

export const useEmployStore = defineStore('employee', () => {
  const loadingStore = useLoadingStore()
  const Employees = ref<Employee[]>([])
  const currentEmployee = ref<Employee>()

  //กำหนดหัวData table
  const initialEmployee: Employee = {
    name: '',
    tel: '',
    status: { id: 1, name: 'FullTime' },
    salary: 0,
    dateIn: '',
    image: 'noimage.jpg',
    files: [],
    branch: null
  }
  //Employeeที่กำลังแก้ไข
  const editedEmployee = ref<Employee>(JSON.parse(JSON.stringify(initialEmployee)))

  //Data Function
  async function getEmployee(id: number) {
    loadingStore.doLoad()
    const res = await employeeService.getEmployee(id)
    editedEmployee.value = res.data
    loadingStore.finish()
    //มีปัญหาการเรียกใช้
  }

  async function getEmployeeByName(id: string) {
    loadingStore.doLoad()
    const res = await employeeService.getEmployeeByName(id)
    editedEmployee.value = res.data
    loadingStore.finish()
    //มีปัญหาการเรียกใช้
  }

  async function getEmployees() {
    console.log('เรียกใช้่')
    loadingStore.doLoad()
    const res = await employeeService.getEmployees()
    Employees.value = res.data
    loadingStore.finish()
    console.log('เรียกใช้เสร็จ')
    //มีปัญหาการเรียกใช้
  }

  async function saveEmployee() {
    loadingStore.doLoad()
    const employee = editedEmployee.value
    if (!employee.id) {
      //Add New
      const res = await employeeService.addEmployee(employee)
    } else {
      //Update
      const res = await employeeService.updateEmployee(employee)
    }
    await getEmployees()
    loadingStore.finish()
  }

  async function deleteEmployee() {
    loadingStore.doLoad()
    const employee = editedEmployee.value
    const res = await employeeService.delEmployee(employee)
    await getEmployees()
    loadingStore.finish()
  }

  function clearForm() {
    editedEmployee.value = JSON.parse(JSON.stringify(initialEmployee))
  }
  //End Data Function

  async function getCurrentEmployee(id: number) {
    try {
      loadingStore.doLoad()
      const res = await employeeService.getEmployee(id)
      currentEmployee.value = res.data
      loadingStore.finish()
    } catch (error) {
      console.log(error)
      loadingStore.finish()
    }
  }

  onMounted(() => {
    getEmployees()
  })

  return {
    onMounted,
    Employees,
    saveEmployee,
    deleteEmployee,
    getEmployees,
    getEmployee,
    editedEmployee,
    clearForm,
    currentEmployee,
    getCurrentEmployee
  }
})
