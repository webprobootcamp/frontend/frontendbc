import type { StoreExp } from '@/types/StoreExpenses'
import { defineStore } from 'pinia'
import { computed, ref } from 'vue'
import { useLoadingStore } from './loading'
import storeexpsService from '@/services/storeexps'

export const useStoreExp = defineStore('storeexpenses', () => {
  const loadingStore = useLoadingStore()
  const StoreExps = ref<StoreExp[]>([])
  console.log(StoreExps)

  //กำหนดหัวData table
  const initialStoreExp: StoreExp = {
    status: [],
    electricBill: 0,
    waterBill: 0,
    due_date: '',
    total: 0,
    branch: {
      id: 1,
      name: 'D-Coffee BUU',
      maneger: 'Kob',
      address: 'Burhapa U, Bangsean',
      tels: '66812345677'
    }
  }
  //StoreExpที่กำลังแก้ไข
  const editedStoreExp = ref<StoreExp>(JSON.parse(JSON.stringify(initialStoreExp)))

  //Data Function
  async function getStoreExp(id: number) {
    loadingStore.doLoad()
    const res = await storeexpsService.getStoreExp(id)
    editedStoreExp.value = res.data
    loadingStore.finish()
  }

  async function getStoreExps() {
    loadingStore.doLoad()
    const res = await storeexpsService.getStoreExps()
    StoreExps.value = res.data
    console.log(res.data)
    console.log(StoreExps.value)
    loadingStore.finish()
  }

  async function saveStoreExp() {
    loadingStore.doLoad()
    const employee = editedStoreExp.value
    if (!employee.id) {
      //Add New
      const res = await storeexpsService.addStoreExp(employee)
    } else {
      //Update
      const res = await storeexpsService.updateStoreExp(employee)
    }
    await getStoreExps()
    console.log('แสดงตอนSave')
    console.log(saveStoreExp)
    loadingStore.finish()
  }

  async function deleteStoreExp() {
    loadingStore.doLoad()
    const employee = editedStoreExp.value
    const res = await storeexpsService.delStoreExp(employee)
    await getStoreExps()
    loadingStore.finish()
  }

  function clearForm() {
    editedStoreExp.value = JSON.parse(JSON.stringify(initialStoreExp))
  }

  //เพิ่มใหม่
  async function setStoreExps(newStoreExps: StoreExp[]) {
    StoreExps.value = newStoreExps
  }
  //End Data Function
  function caltotal() {}

  return {
    StoreExps,
    saveStoreExp,
    deleteStoreExp,
    getStoreExps,
    getStoreExp,
    editedStoreExp,
    clearForm,
    setStoreExps
  }
})
