import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import type { Product } from '@/types/Product'
import type { ReceiptItem } from '@/types/ReceiptItem'
import type { Receipt } from '@/types/Receipt'
import { useAuthStore } from './auth'
import { useMemberStore } from './member'
import { usePromotionStore } from './Promotion'
import { useLoadingStore } from './loading'
import receiptService from '@/services/receipt'
import { useUserStore } from './user'
import { useEmployStore } from './employee'
import Branch from '@/services/Branch'
import { useBranchStore } from './branch'
export const useReceiptStore = defineStore('receipt', () => {
  const promotionStore = usePromotionStore()
  const authStore = useAuthStore()
  const memberStore = useMemberStore()
  const userStore = useUserStore()
  const employeeStore = useEmployStore()
  const receiptDialog = ref(false)
  const DeleteDialog = ref(false)
  const endDialog = ref(false)
  const branchStore = useBranchStore()

  const receipt = ref<Receipt>({
    createdDate: new Date(),
    promotionDiscount: 0,
    memberDiscount: 0,
    totalBefore: 0,
    receivedAmount: 0,
    cashBack: 0,
    totalQty: 0,
    paymentType: 'cash',
    userId: authStore.getCurrentUser()?.id,
    user: authStore.getCurrentUser(),
    branch: authStore.getCurrentBranch(),
    branchId: authStore.getCurrentBranch()?.id,
    memberId: 0,
    get totalDiscount() {
      return this.promotionDiscount + this.memberDiscount
    },
    get total() {
      return this.totalBefore - this.totalDiscount
    },
    promotions: []
  })

  const receiptItems = ref<ReceiptItem[]>([])
  const addReceiptItem = (product: Product) => {
    // the ? sign is called optional chaining to prevent error
    const index = receiptItems.value.findIndex((item) => item.product?.id === product.id)

    if (index >= 0) {
      receiptItems.value[index].unit++
      calReceipt()
      return
    } else {
      const newReceipt: ReceiptItem = {
        name: product.name,
        price: product.price,
        unit: 1,
        productId: product.id!,
        product: product,
        id: 0
      }

      receiptItems.value.push(newReceipt)
    }
    calReceipt()
  }
  const removeReceiptItem = (receiptItem: ReceiptItem) => {
    // the line below is called "callback function" to find index of array
    const index = receiptItems.value.findIndex((item) => item === receiptItem)
    receiptItems.value.splice(index, 1)
    calReceipt()
  }
  function inc(item: ReceiptItem) {
    item.unit++
    calReceipt()
  }
  const dec = (item: ReceiptItem) => {
    if (item.unit === 1) {
      removeReceiptItem(item)
      return
    }
    item.unit--
    calReceipt()
  }

  function calReceipt() {
    let totalBefore = 0
    let totalQty = 0
    for (const item of receiptItems.value) {
      totalBefore = totalBefore + item.price * item.unit
      totalQty = totalQty + item.unit
    }
    receipt.value.totalBefore = totalBefore
    receipt.value.totalQty = totalQty
  }
  function showReceiptDialog() {
    receipt.value.receiptItems = receiptItems.value
    receiptDialog.value = true
  }
  function clear() {
    receiptItems.value = []
    receipt.value = {
      createdDate: new Date(),
      promotionDiscount: 0,
      memberDiscount: 0,
      totalBefore: 0,
      receivedAmount: 0,
      cashBack: 0,
      totalQty: 0,
      paymentType: 'cash',
      userId: authStore.getCurrentUser()?.id,
      user: authStore.getCurrentUser(),
      branch: authStore.getCurrentBranch(),
      branchId: authStore.getCurrentBranch()?.id,
      promotions: [],
      memberId: 0,
      get totalDiscount() {
        return this.promotionDiscount + this.memberDiscount
      },
      get total() {
        return this.totalBefore - this.totalDiscount
      }
    }
    memberStore.clear()
  }
  function showDeleteDialog() {
    receipt.value.receiptItems = receiptItems.value
    DeleteDialog.value = true
  }
  function showendDialog() {
    receipt.value.receiptItems = receiptItems.value
    endDialog.value = true
  }
  const receipts = ref()
  const loadingStore = useLoadingStore()
  async function getReceipts() {
    try {
      loadingStore.doLoad()
      const res = await receiptService.getReceipts()
      const receiptsData = res.data
      receipts.value = receiptsData // Assuming promotions is a ref() or reactive() state variable
      loadingStore.finish()
    } catch (error) {
      console.error(error)
      loadingStore.finish()
    }
  }
  const currentReciept = ref<Receipt>()
  async function getCurrentReciept(id: number) {
    try {
      loadingStore.doLoad()
      const res = await receiptService.getReceipt(id)
      currentReciept.value = res.data
      loadingStore.finish()
    } catch (error) {
      console.log(error)
      loadingStore.finish()
    }
  }
  async function saveReceipt() {
    try {
      loadingStore.doLoad()
      // editedPromotion.value.startdate = startDate.value
      // editedPromotion.value.enddate = endDate.value
      console.log('Create<Post>' + receipt.value)
      console.log('receipt.value = ', receipt.value)
      const res = await receiptService.addReceipt(receipt.value)
      console.log('branch = ' + employeeStore.currentEmployee?.branch?.id)
      await getReceipts()
      loadingStore.finish()
    } catch (error) {
      console.log(error)
      loadingStore.finish()
    }
  }
  const editedReceipt = ref<Receipt>()
  async function getReceipt(id: number) {
    try {
      loadingStore.doLoad()
      const res = await receiptService.getReceipt(id)
      editedReceipt.value = res.data
      loadingStore.finish()
    } catch (error) {
      console.log(error)
      loadingStore.finish()
    }
  }

  async function delReceipt() {
    try {
      loadingStore.doLoad()
      const receipt = editedReceipt.value
      const res = await receiptService.delReceipt(receipt!)

      await getReceipts()
      loadingStore.finish()
    } catch (error) {
      await getReceipts()
      loadingStore.finish()
    }
  }

  return {
    currentReciept,
    receiptItems,
    receipt,
    receiptDialog,
    DeleteDialog,
    endDialog,
    showendDialog,
    addReceiptItem,
    removeReceiptItem,
    inc,
    dec,
    calReceipt,
    showReceiptDialog,
    clear,
    showDeleteDialog,
    getReceipts,
    receipts,
    saveReceipt,
    getReceipt,
    delReceipt
  }
})
