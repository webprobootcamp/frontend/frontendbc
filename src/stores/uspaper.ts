import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import type { USPaperItem } from '@/types/USPaperItem'
import type { USPaper } from '@/types/USPaper'
import { useAuthStore } from './auth'
import itemusService from '@/services/itemus'
import { useLoadingStore } from './loading'
import { useMessageStore } from './message'
import type { Stock } from '@/types/Stock'
import stock from '@/services/stock'
import uspaperService from '@/services/uspaper'
import { useStockStore } from './stock'

export const useUSPaperStore = defineStore('uspaper', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const usestockStore = useStockStore()
  const authStore = useAuthStore()
  const receiptsuse = ref()
  const DeleteDialog = ref(false)
  const usPaperItems = ref<USPaperItem[]>([])
  const usPaper = ref<USPaper>({
    id: 0,
    created: new Date(),
    total: 0,
    amount: 0,
    userId: authStore.getCurrentUser()!.id!,
    user: authStore.getCurrentUser()!
  })
  function initUSPaper() {
    usPaper.value = {
      id: 0,
      created: new Date(),
      total: 0,
      amount: 0,
      userId: authStore.getCurrentUser()!.id!,
      user: authStore.getCurrentUser()!
    }
    usPaperItems.value = []
  }

  watch(
    usPaperItems,
    () => {
      calUSPaper()
    },
    { deep: true }
  )
  const addUSPaperItem = (b: Stock) => {
    const index = usPaperItems.value.findIndex((item) => item.stock?.id === b.id)
    if (index >= 0) {
      usPaperItems.value[index].qty++
      calUSPaper()
      return
    } else {
      const newUSPaperItem: USPaperItem = {
        id: -1,
        name: b.name,
        price: b.price,
        qty: 1,
        unit: b.unit,
        stockId: b.id,
        stock: b
      }
      usPaperItems.value.push(newUSPaperItem)
    }
    calUSPaper()
  }
  const calUSPaper = function () {
    usPaper.value!.total = 0
    usPaper.value!.amount = 0
    for (let i = 0; i < usPaperItems.value.length; i++) {
      usPaper.value!.total += usPaperItems.value[i].price * usPaperItems.value[i].qty
      usPaper.value!.amount += usPaperItems.value[i].qty
    }
  }
  const editedUSPaper = ref<USPaper>(JSON.parse(JSON.stringify(usPaper)))
  async function getUSPaper(id: number) {
    loadingStore.doLoad()
    const res = await uspaperService.getUSPaper(id)
    editedUSPaper.value = res.data
    loadingStore.finish()
  }

  async function getUSPapers() {
    try {
      loadingStore.doLoad()
      const res = await uspaperService.getUSPapers()
      const receiptuseData = res.data
      receiptsuse.value = receiptuseData
      loadingStore.finish()
    } catch (error) {
      console.error(error)
      loadingStore.finish()
    }
  }
  // const addUSPaperItem = (newUSPaperItem: USPaperItem) => {
  //   usPaperItems.value.push(newUSPaperItem)
  // }
  const deleteUSPaperItem = (selectedItem: USPaperItem) => {
    const index = usPaperItems.value.findIndex((item) => item === selectedItem)
    usPaperItems.value.splice(index, 1)
  }
  const incQtyOfUSPaperItem = (selectedItem: USPaperItem) => {
    selectedItem.qty++
    calUSPaper()
  }
  const decQtyOfUSPaperItem = (selectedItem: USPaperItem) => {
    if (selectedItem.qty === 1) {
      deleteUSPaperItem(selectedItem)
      return
    }
    selectedItem.qty--
    calUSPaper()
  }
  const removeItem = (item: USPaperItem) => {
    const index = usPaperItems.value.findIndex((ri) => ri === item)
    usPaperItems.value.splice(index, 1)
  }

  function clearForm() {
    usPaper.value = {
      id: 0,
      created: new Date(),
      total: 0,
      amount: 0,
      userId: authStore.getCurrentUser()!.id!,
      user: authStore.getCurrentUser()!
    }
    usPaperItems.value = []
  }

  function showDeleteDialog() {
    usPaper.value.stockItems = usPaperItems.value
    DeleteDialog.value = true
  }

  const Usestock = async () => {
    try {
      loadingStore.doLoad()
      await itemusService.addItemUS(usPaper.value!, usPaperItems.value)
      initUSPaper()
      loadingStore.finish()
    } catch (e: any) {
      loadingStore.finish()
      messageStore.showMessage(e.message)
    }
  }

  return {
    usPaper,
    DeleteDialog,
    initUSPaper,
    usPaperItems,
    addUSPaperItem,
    incQtyOfUSPaperItem,
    decQtyOfUSPaperItem,
    deleteUSPaperItem,
    removeItem,
    Usestock,
    clearForm,
    showDeleteDialog,
    getUSPaper,
    getUSPapers,
    receiptsuse
  }
})
