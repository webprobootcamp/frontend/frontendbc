import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import checkstockService from '@/services/checkstock'
import stockService from '@/services/stock'
import type { Checkstock } from '@/types/CheckStock'
import type { CheckstockItem } from '@/types/CheckStockItem'
import type { Stock } from '@/types/Stock'
import { useStockStore } from './stock'
import { useAuthStore } from './auth'

export const useCheckStore = defineStore('checkstock', () => {
  const loadingStore = useLoadingStore()
  const stockStore = useStockStore()
  const authStore = useAuthStore()
  const checkstocks = ref<Checkstock[]>([])
  const checkstockss = ref()
  const checkstockItems: { value: CheckstockItem[] } = { value: [] }
  const checkstock = ref<Checkstock>({
    userId: authStore.getCurrentUser()?.id,
    user: authStore.getCurrentUser(),
    checkstockItems: []
  })
  const addCheckStockItem = (checkstock: Stock) => {
    const newCheckStock: CheckstockItem = {
      name: checkstock.name,
      min: checkstock.min,
      balance: checkstock.balance,
      unit: checkstock.unit,
      status: checkstock.status,
      stock: checkstock
    }
    checkstockItems.value.push(newCheckStock)
  }

  async function getCheckstocks() {
    try {
      loadingStore.doLoad()
      const res = await checkstockService.getCheckstocks()
      console.log(res.data)
      const checkstockData = res.data
      checkstockss.value = await checkstockData
      loadingStore.finish()
    } catch (error) {
      console.log(error)
      loadingStore.finish()
    }
  }
  async function saveCheckstock() {
    loadingStore.doLoad()
    checkstockItems.value = []
    const allStocks: Stock[] = stockStore.material
    allStocks.forEach((stock) => {
      addCheckStockItem(stock)
    })
    checkstock.value.checkstockItems = checkstockItems.value
    try {
      const res = await checkstockService.addCheckstock(checkstock.value)
      await getCheckstocks()
    } catch (error) {
      console.error('Error while saving checkstock:', error)
    }

    loadingStore.finish()
  }
  // async function deleteCheckstock() {
  //   loadingStore.doLoad()
  //   const checkstock = editedCheckstock.value
  //   const res = await checkstockService.delCheckstock(checkstock)

  //   await getCheckstocks()
  //   loadingStore.finish()
  // }

  function clearForm() {
    checkstock.value = {
      id: 0,
      userId: authStore.getCurrentUser()?.id,
      user: authStore.getCurrentUser()
    }
    checkstockItems.value = []
  }
  return {
    addCheckStockItem,
    checkstock,
    checkstocks,
    getCheckstocks,
    saveCheckstock,
    clearForm,
    checkstockss
  }
})
