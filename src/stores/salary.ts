import type { Salary } from '@/types/Salary'
import { defineStore } from 'pinia'
import { nextTick, ref } from 'vue'
import { onMounted } from 'vue'
import { useLoadingStore } from './loading'
import salaryService from '@/services/salary'
import { useMessageStore } from './message'
import { id } from 'vuetify/locale'
import type { VForm } from 'vuetify/components'
import type { Employee } from '@/types/Employee'
import { useEmployStore } from './employee'

const employeeStore = useEmployStore();
const loadingeStore = useLoadingStore();
export const useSalaryStore = defineStore('salary', () => {
  const headers = [
    {
      title: 'Salary ID',
      key: 'id',
      value: 'id'
    },
    {
      title: 'Date',
      key: 'pay_date',
      value: 'pay_date'
    },
    {
      title: 'Employee ID',
      key: 'emp_id',
      value: 'emp_id'
    },
    {
      title: 'Employee Name',
      key: 'emp_name',
      value: 'emp_name'
    },
    {
      title: 'Status',
      key: 'type',
      value: 'type'
    },
    {
      title: 'Period',
      key: 'pay_period',
      value: 'pay_period'
    },
    {
      title: 'Pay',
      key: 'pay',
      value: 'pay'
    },
    {
      title: 'Work Hours',
      key: 'work_hour',
      value: 'work_hour'
    }
  ]
  const form = ref(false)
  const refForm = ref<VForm | null>(null)
  const dialog = ref(false)
  const dialogDelete = ref(false)
  const loading = ref(false)
  const initialsalary: Salary = {
    id: -1,
    pay_date: '',
    emp_id: 0,
    emp_name: '',
    type: 1,
    pay_period: 'monthly',
    pay: 0,
    work_hour: 0
  }
  const editedSalary = ref<Salary>(JSON.parse(JSON.stringify(initialsalary)))
  const salaries = ref<Salary[]>([])
  let editedIndex = -1
  function onSubmit() {}

  function setEmpIDName(employee: Employee) {
    loadingeStore.doLoad()
    editedSalary.value.emp_id = employee.id
    editedSalary.value.emp_name = employee.name
    loadingeStore.finish()
  }
  function closeDelete() {
    loadingeStore.doLoad()
    dialogDelete.value = false
    nextTick(() => {
      editedSalary.value = Object.assign({}, initialsalary)
    })
    loadingeStore.finish()
  }
  async function deleteItemConfirm() {
    // Delete item from list
    loadingeStore.doLoad()
    await deletesalary(editedSalary.value)
    closeDelete()
    loadingeStore.finish()
  }
  function editItem(item: Salary) {
    loadingeStore.doLoad()
    editedIndex = salaries.value.indexOf(item)
    editedSalary.value = Object.assign({}, item)
    dialog.value = true
    loadingeStore.finish()
  }
  function deleteItem(item: Salary) {
    loadingeStore.doLoad()
    editedIndex = salaries.value.indexOf(item)
    editedSalary.value = Object.assign({}, item)
    dialogDelete.value = true
    editedIndex = -1
    loadingeStore.finish()
  }
  function closeDialog() {
    loadingeStore.doLoad()
    dialog.value = false
    nextTick(() => {
      editedSalary.value = Object.assign({}, initialsalary)
      editedIndex = -1
    })
    loadingeStore.finish()
  }
  async function save() {
    loadingeStore.doLoad()
    const { valid } = await refForm.value!.validate()
    if (!valid) return
    closeDialog()
    await savesalary(editedSalary.value)
    loadingeStore.finish()
  }

  async function getsalary() {
    loadingeStore.doLoad()
    const res = await salaryService.getSalaries()
    salaries.value = res.data
    loadingeStore.finish()
  }

  async function savesalary(salary: Salary) {
    loadingeStore.doLoad()
    if (salary.id < 0) {
      console.log('Post ' + JSON.stringify(salary))
      const res = await salaryService.addSalary(salary)
    } else {
      console.log('Patch ' + JSON.stringify(salary))
      const res = await salaryService.editSalary(salary)
    }
    await getsalary()
    loadingeStore.finish()
  }

  async function deletesalary(salary: Salary) {
    loadingeStore.doLoad()
    const res = await salaryService.delSalary(salary)
    await getsalary()
    loadingeStore.finish()
  }

  


  onMounted(() => {
    loadingeStore.doLoad()
    getsalary()
    
console.log('sal')
console.log(salaries)
loadingeStore.finish()
  })
  function clearForm() {
    editedSalary.value = JSON.parse(JSON.stringify(initialsalary))
  }
  return {
    headers,
    form,
    dialog,
    dialogDelete,
    loading,
    initialsalary,
    editedSalary,
    editItem,
    closeDialog,
    deleteItem,
    deleteItemConfirm,
    save,
    salaries,
    getsalary,
    savesalary,
    deletesalary,
    closeDelete,
    onMounted,
    setEmpIDName,
    onSubmit
  }
})
