import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

import receiptService from '@/services/receipt'
import type { Product } from '@/types/Product'
import { useMessageStore } from './message'

export const useMostOrderViewStore = defineStore('mostOrder', () => {
  const loadingStore = useLoadingStore()
  const mostOrders = ref()
  async function getMostOrders() {
    loadingStore.doLoad()
    const res = await receiptService.getOrderAvgView()
    mostOrders.value = res.data
    loadingStore.finish()
  }
  return {
    getMostOrders,
    mostOrders
  }
})
