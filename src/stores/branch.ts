import { defineStore } from 'pinia'
import { ref } from 'vue'
import { useRouter } from 'vue-router'
import { useLoadingStore } from './loading'
import type { Branch } from '@/types/Branch'
import branchService from '@/services/Branch'
export const useBranchStore = defineStore('branch', () => {
  const branchs = ref<Branch[]>([])

  const initialBranch: Branch = {
    name: '',
    maneger: '',
    address: '',
    tels: ''
  }
  const editedBranch = ref<Branch>(JSON.parse(JSON.stringify(initialBranch)))
  const currentBranch = ref<Branch>()
  const branchname = ref('')
  const password = ref('')
  const router = useRouter()
  const dialogFailed = ref(false)
  const isLoggedIn = ref(false)
  const loadingStore = useLoadingStore()
  function closeDialog() {
    dialogFailed.value = false
  }

  function getCurrentBranch(): Branch | null | undefined {
    const strBranch = localStorage.getItem('branch')
    if (strBranch === null) return null

    try {
      const parsedBranch = JSON.parse(strBranch)
      if (
        parsedBranch &&
        parsedBranch.name &&
        parsedBranch.manager &&
        parsedBranch.address &&
        parsedBranch.tels
      ) {
        return parsedBranch
      } else {
        console.error('Stored branch object is missing required properties.')
        return null
      }
    } catch (error) {
      console.error('Error parsing stored branch object:', error)
      return null
    }
  }

  async function getBranch(id: number) {
    loadingStore.doLoad()
    const res = await branchService.getBranch(id)
    editedBranch.value = res.data
    loadingStore.finish()
  }

  async function getBranchs() {
    loadingStore.doLoad()
    const res = await branchService.getBranchs()
    branchs.value = res.data
    loadingStore.finish()
  }

  async function saveBranch() {
    loadingStore.doLoad()
    const branch = editedBranch.value
    if (!branch.id) {
      // Add new
      console.log('Post' + JSON.stringify(branch))
      const res = await branchService.addBranch(branch)
    } else {
      // Update
      console.log('Patch' + JSON.stringify(branch))
      const res = await branchService.updateBranch(branch)
    }

    await getBranchs()
    loadingStore.finish()
  }

  async function deleteBranch() {
    loadingStore.doLoad()
    const branch = editedBranch.value
    const res = await branchService.delBranch(branch)

    await getBranchs()
    loadingStore.finish()
  }

  function clearForm() {
    editedBranch.value = JSON.parse(JSON.stringify(initialBranch))
  }

  return {
    branchs,
    branchname,
    password,
    dialogFailed,
    closeDialog,
    isLoggedIn,
    getBranchs,
    saveBranch,
    deleteBranch,
    editedBranch,
    getBranch,
    clearForm,
    currentBranch,
    getCurrentBranch
  }
})
