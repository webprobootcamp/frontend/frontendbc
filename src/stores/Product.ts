import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

import productService from '@/services/product'
import type { Product } from '@/types/Product'
import { useMessageStore } from './message'

export const useProductStore = defineStore('product', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const products = ref<Product[]>([])
  const initialProduct: Product & { files: File[] } = {
    name: '',
    price: 0,
    type: { id: 1, name: 'Drink' },
    image: 'noimage.jpg',
    files: []
  }

  function findProductsByType(products: any, typeName: any): Product[] | null {
    const foundProduct = products.value.find((product: Product) => product.type.name === typeName)
    if (foundProduct !== undefined) {
      return [foundProduct]
    } else {
      return null
    }
  }

  const productsDrink = findProductsByType(products, 'Drink')
  const productsFood = findProductsByType(products, 'Food')
  const productsDessert = findProductsByType(products, 'Dessert')
  const productsOther = findProductsByType(products, 'Other')

  const editedProduct = ref<Product>(JSON.parse(JSON.stringify(initialProduct)))

  const searchProduct = (productId: number): Product | null => {
    if (productId < 0) {
      return null
    }

    const foundProduct = products.value.find((product) => product.id === productId)
    return foundProduct || null
  }

  async function getProduct(id: number) {
    try {
      loadingStore.doLoad()
      const res = await productService.getProduct(id)
      editedProduct.value = res.data
      loadingStore.finish()
    } catch (e: any) {
      loadingStore.finish()
      messageStore.showMessage(e.message)
    }
  }
  async function getProducts() {
    try {
      loadingStore.doLoad()
      const res = await productService.getProducts()
      console.log(res.data)
      products.value = res.data

      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }
  async function saveProduct() {
    try {
      loadingStore.doLoad()
      const product = editedProduct.value
      if (!product.id) {
        // Add new
        console.log('Post ' + JSON.stringify(product))
        const res = await productService.addProduct(product)
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(product))
        const res = await productService.updateProduct(product)
      }

      await getProducts()
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }
  async function deleteProduct() {
    loadingStore.doLoad()
    const product = editedProduct.value
    const res = await productService.delProduct(product)

    await getProducts()
    loadingStore.finish()
  }

  function clearForm() {
    editedProduct.value = JSON.parse(JSON.stringify(initialProduct))
  }
  return {
    products,
    getProducts,
    saveProduct,
    deleteProduct,
    editedProduct,
    getProduct,
    clearForm,
    searchProduct,
    productsDrink,
    productsFood,
    productsDessert,
    productsOther
  }
})
