import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Stock } from '@/types/Stock'
import StockService from '@/services/stock'
import { useLoadingStore } from './loading'

export const useStockUseStore = defineStore('stockbuy', () => {
  const loadingStore = useLoadingStore()

  const stocks = ref<Stock[]>([])

  async function getStocks() {
    try {
      loadingStore.doLoad()
      const res = await StockService.getStocks()
      stocks.value = res.data

      loadingStore.finish()
    } catch (e) {
      console.log('Error')
      loadingStore.finish()
    }
  }
  return { stocks, getStocks }
})
