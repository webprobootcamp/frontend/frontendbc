import http from './http'
import type { StoreExp } from '@/types/StoreExpenses'

function addStoreExp(storeexpenses: StoreExp) {
  return http.post('/storeexpenses', storeexpenses)
}

function updateStoreExp(storeexpenses: StoreExp) {
  return http.patch(`/storeexpenses/${storeexpenses.id}`, storeexpenses)
}

function delStoreExp(storeexpenses: StoreExp) {
  return http.delete(`/storeexpenses/${storeexpenses.id}`)
}

function getStoreExp(id: number) {
  return http.get(`/storeexpenses/${id}`)
}

function getStoreExps() {
  return http.get('/storeexpenses')
}

export default { addStoreExp, updateStoreExp, delStoreExp, getStoreExp, getStoreExps }
