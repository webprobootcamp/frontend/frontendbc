import type { USPaper } from '@/types/USPaper'
import http from './http'

function addUSPaper(uspaper: USPaper) {
  return http.post(`/usestock-orders`, uspaper)
}

function updateUSPaper(uspaper: USPaper) {
  return http.patch(`/usestock-orders/${uspaper.id}`, uspaper)
}

function delUSPaper(uspaper: USPaper) {
  return http.delete(`/usestock-orders/${uspaper.id}`)
}

function getUSPaper(id: number) {
  return http.get(`/usestock-orders/${id}`)
}

function getUSPapers() {
  return http.get('/usestock-orders')
}

export default { addUSPaper, updateUSPaper, delUSPaper, getUSPaper, getUSPapers }
