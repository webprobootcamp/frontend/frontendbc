import type { BSPaper } from '@/types/BSPaper'
import http from './http'

function addBSPaper(bspaper: BSPaper) {
  return http.post(`/buystock-orders`, bspaper)
}

function updateBSPaper(bspaper: BSPaper) {
  return http.patch(`/buystock-orders/${bspaper.id}`, bspaper)
}

function delBSPaper(bspaper: BSPaper) {
  return http.delete(`/buystock-orders/${bspaper.id}`)
}

function getBSPaper(id: number) {
  return http.get(`/buystock-orders/${id}`)
}

function getBSPapers() {
  return http.get('/buystock-orders')
}

export default { addBSPaper, updateBSPaper, delBSPaper, getBSPaper, getBSPapers }
