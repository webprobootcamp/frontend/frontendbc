import type { Status } from '@/types/Status'
import http from './http'

function addStatus(status: Status) {
  return http.post(`/status`, status)
}

function updateStatus(status: Status) {
  return http.patch(`/status/${status.id}`, status)
}

function delStatus(status: Status) {
  return http.delete(`/status/${status.id}`)
}

function getStatus(id: number) {
  return http.get(`/status/${id}`)
}

function getStatuss() {
  return http.get('/status')
}

export default { addStatus, updateStatus, delStatus, getStatus, getStatuss }
