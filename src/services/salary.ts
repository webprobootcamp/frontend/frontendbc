import { defineStore } from 'pinia'
import http from './http'
import type { Salary } from '@/types/Salary'

function addSalary(Salary: Salary) {
  return http.post('/salaries', Salary)
}

function editSalary(Salary: Salary) {
  return http.patch(`/salaries/${Salary.id}`, Salary)
}

function delSalary(Salary: Salary) {
  return http.delete(`/salaries/${Salary.id}`)
}

function getSalary(id: number) {
  return http.get(`/salaries/${id}`)
}

function getSalaries() {
  return http.get('/salaries')
}
export default {
  addSalary,
  editSalary,
  delSalary,
  getSalary,
  getSalaries
}
