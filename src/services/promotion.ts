import type { Promotion } from '@/types/Promotion'
import http from './http'

function addPromotion(Promotion: Promotion) {
  return http.post(`/Promotions`, Promotion)
}

function updatePromotion(Promotion: Promotion) {
  return http.post(`/Promotions/${Promotion.id}`, Promotion)
}

function delPromotion(Promotion: Promotion) {
  return http.delete(`/Promotions/${Promotion.id}`)
}

function getPromotion(id: number) {
  return http.get(`/Promotions/${id}`)
}

function getPromotions() {
  return http.get('/Promotions')
}

export default { addPromotion, updatePromotion, delPromotion, getPromotion, getPromotions }
