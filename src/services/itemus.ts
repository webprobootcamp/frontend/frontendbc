import type { USPaper } from '@/types/USPaper'
import http from './http'
import type { USPaperItem } from '@/types/USPaperItem'

type USPaperDto = {
  usestockOrderItems: {
    productId: number
    qty: number
  }[]
  userId: number
}

function addItemUS(usPaper: USPaper, usPaperItems: USPaperItem[]) {
  const uspaperDto: USPaperDto = {
    usestockOrderItems: [],
    userId: 0
  }

  uspaperDto.userId = usPaper.userId
  uspaperDto.usestockOrderItems = usPaperItems.map((item) => {
    return {
      productId: item.stockId,
      qty: item.qty
    }
  })
  http.post('/usestock-orders', uspaperDto)
}

export default { addItemUS }
