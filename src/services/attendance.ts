import type { Employee } from '@/types/Employee'
import http from './http'
// clockIn function
function clockIn(employee: Employee) {
  console.log(
    JSON.stringify({
      employeeId: employee.id + '',
      // employeeId: employee.id,
      date: '',
      name: '',
      clockIn: '',
      clockOut: '',
      workedTime: ''
    })
  )
  return http.post(`/attendance`, {
    employeeId: employee.id + '',
    // employeeId: employee.id,
    date: '',
    name: '',
    clockIn: '',
    clockOut: '',
    status: '',
    workedTime: ''
  })
}
async function clockOut(id: number) {
  return http.patch(`/attendance/clockOut/${id}`)
}
function getAttendance(id: number) {
  return http.get(`/attendance/${id}`)
}

function getAllAttendance() {
  return http.get('/attendance')
}
function getAttendanceByEmployeeId(employeeId: number) {
  return http.get(`/attendance/attendancebyemployee/${employeeId}`)
}

export default { clockIn, clockOut, getAttendance, getAllAttendance, getAttendanceByEmployeeId }
