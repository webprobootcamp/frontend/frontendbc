import type { InsertEmp } from '@/types/InsertEmp'
import http from './http'

function addInsertEmp(insertemp: InsertEmp) {
  return http.post(`/employees`, insertemp)
}

function updateInsertEmp(insertemp: InsertEmp) {
  return http.post(`/employees/${insertemp.id}`, insertemp)
}

function delInsertEmpt(insertemp: InsertEmp) {
  return http.delete(`/employees/${insertemp.id}`)
}

function getInsertEmpt(id: number) {
  return http.get(`/employees/${id}`)
}

function getInsertEmp() {
  return http.get('/employees')
}

function getAllspp(
  tel: string,
  name: string,
  salary: number,
  dataIn: string,
  branch: string,
  status: string,
  result: number
) {
  console.log({
    tel: tel,
    name: name,
    salary: salary + '',
    dateIn: dataIn,
    branch: branch,
    status: status,
    result: result
  })
  return http.post('/employees/storeEmploy', {
    tel: tel,
    name: name,
    salary: salary + '',
    dateIn: dataIn,
    branch: branch,
    status: status,
    result: result
  })
}

export default {
  addInsertEmp,
  updateInsertEmp,
  delInsertEmpt,
  getInsertEmpt,
  getInsertEmp,
  getAllspp
}
