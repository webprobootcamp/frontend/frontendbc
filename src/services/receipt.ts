import http from './http'
import type { Receipt } from '@/types/Receipt'

function addReceipt(receipt: Receipt) {
  return http.post(`/receipts`, receipt)
}

function updateReceipt(receipt: Receipt) {
  return http.post(`/receipts/${receipt.id}`, receipt)
}

function delReceipt(receipt: Receipt) {
  return http.delete(`/receipts/${receipt.id}`)
}

function getReceipt(id: number) {
  return http.get(`/receipts/${id}`)
}

function getReceipts() {
  return http.get('/receipts')
}

function getOrderAvgView() {
  return http.get('/receipts/getView')
}

function getReceiptInRangeDate(type: string, startDate: string, endDate: string): Promise<any> {
  // Adjust the URL and parameters as needed
  return http.get(`/receipts/ReceiptInRangeDate?type=${type}&startDate=${startDate}&endDate=${endDate}`)
    .then(response => response.data);
}


export default { addReceipt, updateReceipt, delReceipt, getReceipt, getReceipts, getOrderAvgView ,getReceiptInRangeDate }
