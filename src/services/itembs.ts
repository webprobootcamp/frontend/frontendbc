import type { BSPaper } from '@/types/BSPaper'
import http from './http'
import type { BSPaperItem } from '@/types/BSPaperItem'

type BSPaperDto = {
  buystockOrderItems: {
    productId: number
    qty: number
  }[]
  userId: number
}

function addItemBS(bsPaper: BSPaper, bsPaperItems: BSPaperItem[]) {
  const bspaperDto: BSPaperDto = {
    buystockOrderItems: [],
    userId: 0
  }
  bspaperDto.userId = bsPaper.userId
  bspaperDto.buystockOrderItems = bsPaperItems.map((item) => {
    return {
      productId: item.stockId,
      qty: item.qty
    }
  })
  http.post('/buystock-orders', bspaperDto)
}

export default { addItemBS }
