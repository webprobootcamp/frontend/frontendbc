import type { CountEmp } from '@/types/CountEmp'
import http from './http'

function addCountEmp(countepmtemp: CountEmp) {
  return http.post(`/employees`, countepmtemp)
}

function updateCountEmp(countepmtemp: CountEmp) {
  return http.post(`/employees/${countepmtemp.id}`, countepmtemp)
}

function delCountEmp(countepmtemp: CountEmp) {
  return http.delete(`/employees/${countepmtemp.id}`)
}

function getCountEmp(id: number) {
  return http.get(`/employees/${id}`)
}

function getCountEmps() {
  return http.get('/employees')
}
function getAllsppp(
  tel: string,
  name: string,
  salary: number,
  dataIn: string,
  branch: string,
  status: string,
  total: number
) {
  console.log({
    tel: tel,
    name: name,
    salary: salary + '',
    dateIn: dataIn,
    branch: branch,
    status: status,
    total: total
  })
  return http.post('/employees/storeCountEmploy', {
    tel: tel,
    name: name,
    salary: salary + '',
    dateIn: dataIn,
    branch: branch,
    status: status,
    total: total
  })
}

export default { addCountEmp, updateCountEmp, delCountEmp, getAllsppp, getCountEmp, getCountEmps }
