import type { Checkstock } from '@/types/CheckStock'
import http from './http'

function addCheckstock(checkstock: Checkstock) {
  return http.post('/checkstock', checkstock)
}

function updateCheckstock(checkstock: Checkstock) {
  return http.patch(`/checkstock/${checkstock.id}`, checkstock)
}

function delCheckstock(checkstock: Checkstock) {
  return http.delete(`/checkstock/${checkstock.id}`)
}

function getCheckstock(id: number) {
  return http.get(`/checkstock/${id}`)
}

function getCheckstocks() {
  return http.get('/checkstock')
}

export default { addCheckstock, updateCheckstock, delCheckstock, getCheckstock, getCheckstocks }
