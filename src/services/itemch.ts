import type { Checkstock } from '@/types/CheckStock'
import http from './http'
import type { CheckstockItem } from '@/types/CheckStockItem'

type CheckstockDto = {
  checkstockItems: {
    stockId: number
    qty: number
  }[]
  userId: number
}

function addItemCH(chstock: Checkstock, chstockItems: CheckstockItem[]) {
  const checkStockDto: CheckstockDto = {
    checkstockItems: [],
    userId: 0
  }
  checkStockDto.checkstockItems = chstockItems.map((item) => {
    return {
      productId: item.stock,
      qty: item.balance
    }
  })
  http.post('/checkstock', checkStockDto)
}

export default { addItemCH }
