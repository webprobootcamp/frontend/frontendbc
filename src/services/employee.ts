import type { Employee } from '@/types/Employee'
import http from './http'

function addEmployee(employee: Employee & { files: File[] }) {
  const formData = new FormData()
  formData.append('name', employee.name)
  formData.append('salary', employee.salary.toString())
  formData.append('tel', employee.tel)
  formData.append('min', employee.dateIn)
  formData.append('unit', employee.dateIn)
  formData.append('status', JSON.stringify(employee.status))
  if (employee.files && employee.files.length > 0) formData.append('file', employee.files[0])
  return http.post('/employees', formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function updateEmployee(employee: Employee & { files: File[] }) {
  const formData = new FormData()
  formData.append('name', employee.name)
  formData.append('salary', employee.salary.toString())
  formData.append('tel', employee.tel)
  formData.append('min', employee.dateIn)
  formData.append('unit', employee.dateIn)
  formData.append('status', JSON.stringify(employee.status))
  if (employee.files && employee.files.length > 0) formData.append('file', employee.files[0])
  return http.post(`/employees/${employee.id}`, formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function delEmployee(employee: Employee) {
  return http.delete(`/employees/${employee.id}`)
}

function getEmployee(id: number) {
  return http.get(`/employees/${id}`)
}

function getEmployeeByName(id: string) {
  return http.get(`/employees/${id}`)
}

function getEmployees() {
  return http.get('/employees')
}
function getAllspp() {
  return http.get('/employees/storeEmploy')
}

export default { addEmployee, updateEmployee, delEmployee, getEmployee, getEmployees, getAllspp,  getEmployeeByName}
