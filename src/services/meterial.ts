import type { Material } from '@/types/Material'
import http from './http'

type typeMat = { status: String; unit: Number }
function addMaterial(selectedMat: Material) {
  return http.post(`/stocks`, selectedMat)
}

function updateMaterial(selectedMat: Material) {
  return http.post(`/stocks/${selectedMat.id}`, selectedMat)
}

function delMaterial(selectedMat: Material) {
  return http.delete(`/stocks/${selectedMat.id}`)
}

function getMaterial(id: number) {
  return http.get(`/stocks/${id}`)
}

function getMaterials() {
  return http.get('/stocks')
}

function getAllspp(statusm: typeMat) {
  console.log(statusm.status, statusm.unit)
  return http.post('/stocks/storeStock', statusm)
}

export default { addMaterial, getAllspp, getMaterial, updateMaterial, delMaterial, getMaterials }
