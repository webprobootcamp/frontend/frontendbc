import type { Branch } from './Branch'
import type { Employee } from './Employee'
import type { ReportExpensesItem } from './ReportExpensesItem'

type ReportExpenses = {
  id: number
  createdDate: Date
  branch?: Branch
  employees?: Employee
  reportexpensesitem?: ReportExpensesItem[]
}

export type { ReportExpenses }
