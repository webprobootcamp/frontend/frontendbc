import type { Branch } from './Branch'
import type { Employee } from './Employee'

type Status = 'Successful' | 'Pending'
type StoreExp = {
  id?: number
  status: Status[]
  electricBill: number
  waterBill: number
  total: number
  due_date: string
  branch: Branch
}

export type { Status, StoreExp }
