import type { Stock } from './Stock'

const defaultUSPaperItem = {
  id: -1,
  name: '',
  price: 0,
  qty: 0,
  unit: '',
  usestockId: -1,
  usestock: null
}
type USPaperItem = {
  id: number
  name: string
  price: number
  qty: number
  unit: string
  stockId?: number
  stock?: Stock
}

export { type USPaperItem, defaultUSPaperItem }
