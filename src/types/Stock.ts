import type { Branch } from './Branch'

type Status = 'Available' | 'Low' | 'Out of Stock'

type Stock = {
  id?: number
  name: string
  price: number
  image: string
  status: Status
  min: number
  balance: number
  unit: string
  branch: Branch
  files: []
}
export type { Status, Stock }
