import type { USPaperItem } from './USPaperItem'
import type { User } from './User'

const defaultUSPaper = {
  id: -1,
  createdDate: new Date(),
  total: 0,
  paymentType: 'cash',
  employeeId: 1
}
type USPaper = {
  id?: number
  created: Date
  total: number
  amount: number
  userId: number
  usestockorderItems?: USPaperItem[]
  user?: User
}

export { type USPaper }
