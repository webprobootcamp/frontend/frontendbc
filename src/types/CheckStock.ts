import type { CheckstockItem } from './CheckStockItem'
import type { User } from './User'

export type Checkstock = {
  id?: number
  userId?: number
  user?: User | null
  checkstockItems: CheckstockItem[]
}
