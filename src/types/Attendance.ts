import type { Employee } from './Employee'

export type Attendance = {
  id?: number
  employee: Employee
  name: string
  date: Date
  clockIn: string
  clockOut: string
  status: string
  workedTime: number
}
