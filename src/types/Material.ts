type Material = {
  id?: number
  material_name: string
  num_of_materials: number
  material_status: string
  last_purchase_price: number
}
export type { Material }
