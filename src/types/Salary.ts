type Salary = {
  id: number
  pay_date: string
  emp_id?: number
  emp_name: string
  type: number
  pay_period: string
  pay: number
  work_hour: number
}

export type { Salary }
