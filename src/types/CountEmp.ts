type CountEmp = {
  id?: number
  name: string
  tel: string
  status: string
  salary: number
  dateIn: string
  branch: string
}

export type { CountEmp }
