import type { Employee } from './Employee'
import type { Role } from './Role'

type Gender = 'male' | 'female' | 'others'
type User = {
  id?: number
  email: string
  password: string
  name: string
  image: string
  gender: Gender // Male, Female, Others
  role: Role
  employee: Employee | null
}

export type { Gender, User }
