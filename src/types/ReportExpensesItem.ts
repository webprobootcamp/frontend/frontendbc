import type { StoreExp } from './StoreExpenses'

type ReportExpensesItem = {
  id: number
  name: string
  total: number
  price: number
  due_date: string
  status: string
  storeExpId: number
  storeExp?: StoreExp
}

export type { ReportExpensesItem }
