import type { Type } from './Type'
type Product = {
  id?: number
  name: string
  price: number
  type: Type
  image: string
  files: []
}
function getImageUrl(product: Product) {
  return 'http://localhost:4000/images/products/' + product.image
}
export { type Product, getImageUrl }
