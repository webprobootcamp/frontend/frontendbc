type Member = {
  id?: number
  name: string
  tel: string
  point: number
  spent: number
}

export type { Member }
