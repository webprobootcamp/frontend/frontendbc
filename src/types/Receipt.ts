import type { Branch } from './Branch'
import type { Member } from './Member'
import type { Promotion } from './Promotion'
import type { ReceiptItem } from './ReceiptItem'
import type { User } from './User'

type Receipt = {
  id?: number
  createdDate: Date
  totalBefore: number
  memberDiscount: number
  promotionDiscount: number
  totalDiscount: number
  total: number
  receivedAmount: number
  cashBack: number
  totalQty: number
  paymentType: string
  userId?: number
  user?: User | null
  memberId: number
  member?: Member
  branch?: Branch | null
  branchId?: number
  receiptItems?: ReceiptItem[]
  promotions: Promotion[]
}

export type { Receipt }
