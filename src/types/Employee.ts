import type { Branch } from './Branch'
import type { Status } from './Status'

type Employee = {
  id?: number
  name: string
  tel: string
  status: Status
  salary: number
  dateIn: string
  image: string
  files: []
  branch: Branch | null
}

export type { Employee }
