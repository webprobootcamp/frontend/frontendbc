import type { Stock } from './Stock'

export type CheckstockItem = {
  id?: number
  name: string
  min: number
  balance: number
  status: string
  unit: string
  stock?: Stock
}
